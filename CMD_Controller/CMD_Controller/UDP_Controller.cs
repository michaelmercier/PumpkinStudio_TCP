﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace CMD_Controller
{
    public class UDP_Controller
    {
        UdpClient udpClient = new UdpClient();

        public void SendData(string sValue, string ip)
        {    
            udpClient.Connect(ip, 80);
            Byte[] senddata = Encoding.ASCII.GetBytes(sValue);
            udpClient.Send(senddata, senddata.Length);
        }
    }
}
