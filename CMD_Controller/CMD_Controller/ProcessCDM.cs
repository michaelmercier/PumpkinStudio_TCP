﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMD_Controller
{
    public class ProcessCDM
    {    
        Process cmdProcess;
        StreamWriter cmdStreamWriter;    

        private static StringBuilder cmdOutput = null;
        public string EXECUTE_GAME;
        public string KILL_GAME;
        private string GamePath;
        private string GameName;
        private string LanguagePath;

        /// <summary>
        /// CMD informations and initialization. 
        /// </summary>
        public void StartCMD()
        {
            cmdOutput = new StringBuilder("");
            cmdProcess = new Process();

            cmdProcess.StartInfo.FileName = "cmd.exe";
            cmdProcess.StartInfo.UseShellExecute = false;
            cmdProcess.StartInfo.CreateNoWindow = true;
            cmdProcess.StartInfo.RedirectStandardOutput = true;

            cmdProcess.OutputDataReceived += new DataReceivedEventHandler(SortOutputHandler);
            cmdProcess.StartInfo.RedirectStandardInput = true;
            cmdProcess.Start();

            cmdStreamWriter = cmdProcess.StandardInput;
            cmdProcess.BeginOutputReadLine();
        }


        public void ExecuteCommands(string sCommand)
        {
            cmdStreamWriter.WriteLine(sCommand);
        }

        /// </summary>
        /// Output of the CMD.
        /// </summary>
        private static void SortOutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            if (!String.IsNullOrEmpty(outLine.Data))
            {
                cmdOutput.Append(Environment.NewLine + outLine.Data);
            }
        }


        /// <summary>
        /// Read file and return the 2 paths required. (1. Game Location 2. Language location)
        /// </summary>
        public string[] ReadFileGamePath()
        {
            string line;
            string[] lines;
            string sFullPath = Path.GetFullPath("GamePath.txt");

            var list = new List<string>();
            var fileStream = new FileStream(sFullPath, FileMode.Open, FileAccess.Read);
            
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                //line = streamReader.ReadLine();
                while ((line = streamReader.ReadLine()) != null)
                {
                    list.Add(line);
                }
            }

            lines = list.ToArray();

            GamePath = lines[0];
            LanguagePath = lines[1];
            
            return lines;    
        }

        /// <summary>
        /// Initialize value with the good game path and name that is into the gamepath.txt 
        /// </summary>
        public void InitializeValues()
        {
            EXECUTE_GAME = "start /max " + GamePath;

            char[] delimiterChars = { '\\' };
            string[] GamePathName = GamePath.Split(delimiterChars);

            GameName = ConvertStringArrayToString(GamePathName);
            KILL_GAME = "Taskkill /F /IM " + GameName; 
        }

        /// <summary>
        /// Taking the only array for the name of the game.
        /// </summary>
        public string ConvertStringArrayToString(string[] array)
        {
            string builder = "";
            foreach (string value in array)
            {
                builder = value;
            }
            return builder;
        }
    }
}
