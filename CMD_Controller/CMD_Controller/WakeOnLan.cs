﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CMD_Controller
{
    public class WakeOnLan
    {
        private const string IP_ADDRESS = "255.255.255.255";
        private string[] MacAddressList;

        public void WakeUp()
        {
            ReadMacAdressFile();

            foreach (var mac in MacAddressList)
            {
                IPEndPoint pEndPoint = new IPEndPoint(IPAddress.Parse(IP_ADDRESS), 9);

                //將aa:bb:cc:dd:ee:ff或aa-bb-cc-dd-ee-ff MAC地址轉成byte[]  
                byte[] macAddrBytes = mac.Split('-', ':').Select(o => Convert.ToByte(o, 16)).ToArray();

                using (UdpClient udpClient = new UdpClient())
                {
                    try
                    {
                        byte[] data = new byte[102];
                        //最前方六個0xff
                        for (var i = 0; i < 6; i++)
                            data[i] = 0xff;

                        //重複16次MAC地址

                        for (int j = 1; j <= 16; j++)
                        {
                            macAddrBytes.CopyTo(data, j * 6);
                        }

                        udpClient.Send(data, (int)data.Length, pEndPoint);
                        udpClient.Close();
                    }
                    catch
                    { }
                }
            }
        }

        private void ReadMacAdressFile()
        {
            var list = new List<string>();
            var fileStream = new FileStream(System.IO.Path.GetFullPath("MacAddress.txt"), FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    list.Add(line);
                }
            }
            MacAddressList = list.ToArray();
        }
    }
}

