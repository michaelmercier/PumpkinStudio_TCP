﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;

namespace CMD_Controller
{
    public partial class SettingsWindow : Window
    {
        private const string SHUTDOWN = "Shutdown";
        private const string OPENMUMBLE = "start /min mumble://computer";
        //private const string OPENMUMBLE = "start mumble://computer";
        private const string KILLMUMBLE = "taskkill /s localhost /IM mumble.exe";

        private bool TeamSpeak = false;

        public int DefaultVideoPlayer = 0;
        private int DefaultChatServer = 0;

        private MainWindow main;
        TCP_Controller tcp = new TCP_Controller();
        ProcessCDM cmd = new ProcessCDM();
        WakeOnLan wake = new WakeOnLan();

        public SettingsWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cmd.StartCMD();
        }

        private void btnReturn_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

        public void ReceiveMainWindow(MainWindow m)
        {
            main = m;
        }

        private void bShutDown_Click(object sender, RoutedEventArgs e)
        {
            imgShutdown.Source = new BitmapImage(new Uri(@"/Images/Second_Button_Close_Blue.png", UriKind.Relative));
            MessageBoxResult result = MessageBox.Show("Close all client's computers ?", "Shutdown", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                imgOpenPC.Source = new BitmapImage(new Uri(@"/Images/Second_Button_Open_Black.png", UriKind.Relative));
                for (int i = 0; i < main.nbIP; i++)
                {
                    tcp.MainTCP(SHUTDOWN, main.IpList[i]);
                }
            }
            else
                imgShutdown.Source = new BitmapImage(new Uri(@"/Images/Second_Button_Close_Black.png", UriKind.Relative));
        }

        /// <summary>
        /// Open chat
        /// </summary>
        private void btnTeamSpeak_Click(object sender, RoutedEventArgs e)
        {
            if (!TeamSpeak)
            {
                for (int i = 0; i < main.IpList.Length; i++)
                {
                    try
                    {
                        tcp.MainTCP(OPENMUMBLE + i + "@" + main.IpList[DefaultChatServer], main.IpList[i]);
                    }
                    catch { }
                }
                imgTeamSpeak.Source = new BitmapImage(new Uri(@"/Images/Second_Button_Chat_Blue.png", UriKind.Relative));
                TeamSpeak = true;
            }
            else
            {
                for (int i = 0; i < main.IpList.Length; i++)
                {
                    tcp.MainTCP(KILLMUMBLE, main.IpList[i]);
                }
                imgTeamSpeak.Source = new BitmapImage(new Uri(@"/Images/Second_Button_Chat_Black.png", UriKind.Relative));
                TeamSpeak = false;
            }
        }

        /// <summary>
        /// Button to set the video that need to opens on which computer.
        /// </summary>
        private void btnVideoSelection1_Click(object sender, RoutedEventArgs e)
        {
            DefaultVideoPlayer++;

            if (DefaultVideoPlayer == 6)
            {
                DefaultVideoPlayer = 0;
                txtVideo1.Text = "P" + (0).ToString();
            }

            string GetText = txtVideo1.Text;
            string[] SplitNumber = GetText.Split('P');

            switch (Convert.ToInt32(SplitNumber[1]))
            {
                case 0:
                    txtVideo1.Text = "P" + (DefaultVideoPlayer).ToString();
                    break;
                case 1:
                    txtVideo1.Text = "P" + (DefaultVideoPlayer).ToString();
                    break;
                case 2:
                    txtVideo1.Text = "P" + (DefaultVideoPlayer).ToString();
                    break;
                case 3:
                    txtVideo1.Text = "P" + (DefaultVideoPlayer).ToString();
                    break;
                case 4:
                    txtVideo1.Text = "P" + (DefaultVideoPlayer).ToString();
                    break;
            }

            if (DefaultVideoPlayer == 5)
            {
                txtVideo1.Text = "All";
            }
        }

        private void btnVideoSelection1_Loaded(object sender, RoutedEventArgs e)
        {
            txtVideo1.Text = "P"+(DefaultVideoPlayer).ToString();
        }

        private void bCloseApp_Click(object sender, RoutedEventArgs e)
        {
            main.Close();
            this.Close();
        }

        private void btnOpenComputer_Click(object sender, RoutedEventArgs e)
        {
            wake.WakeUp();
            imgShutdown.Source = new BitmapImage(new Uri(@"/Images/Second_Button_Close_Black.png", UriKind.Relative));
            imgOpenPC.Source = new BitmapImage(new Uri(@"/Images/Second_Button_Open_Blue.png", UriKind.Relative));
        }

        private void btnChatSelection_Click(object sender, RoutedEventArgs e)
        {
            if (DefaultChatServer == 3)
                DefaultChatServer = 0;
            else
            DefaultChatServer++;

            switch (DefaultChatServer)
            {
                case 0:
                    txtChat.Text = "P" + (DefaultChatServer+1).ToString();
                    break;
                case 1:
                    txtChat.Text = "P" + (DefaultChatServer+1).ToString();
                    break;
                case 2:
                    txtChat.Text = "P" + (DefaultChatServer+1).ToString();
                    break;
                case 3:
                    txtChat.Text = "P" + (DefaultChatServer+1).ToString();
                    break;
            }
        }

        private void btnChatSelection_Loaded(object sender, RoutedEventArgs e)
        {
            txtChat.Text = "P" + (DefaultChatServer+1).ToString();
        }
    }
}
