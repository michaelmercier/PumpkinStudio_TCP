﻿using System;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Globalization;
using System.Threading.Tasks;

namespace CMD_Controller
{
    public class TCP_Controller
    {
        const int PORT_NO = 5000;
        private MainWindow main;

        public bool MainTCP(string sData, string ip)
        {
            try
            {
                string textToSend = sData;
                IPAddress ipaddress = IPAddress.Parse(ip);

                string endPoint = ip + ":" + PORT_NO;

                IPEndPoint hostEndPoint = ConvertStringToIPEndPoint(endPoint);
                   
                int connectTimeoutMilliseconds = 100;

                // Test the connection
                TcpClient tcpClient = new TcpClient();
                var connectionTask = tcpClient.ConnectAsync(ip, PORT_NO).ContinueWith(task => {
                return task.IsFaulted ? null : tcpClient;
                    }, TaskContinuationOptions.ExecuteSynchronously);
                var timeoutTask = Task.Delay(connectTimeoutMilliseconds).ContinueWith<TcpClient>(task => null, TaskContinuationOptions.ExecuteSynchronously);
                var resultTask = Task.WhenAny(connectionTask, timeoutTask).Unwrap();

                resultTask.Wait();
                var resultTcpClient = resultTask.Result;

                if (resultTcpClient != null)
                {
                    // Connected!
                    Console.WriteLine("TCP available : " + hostEndPoint);
                    NetworkStream nwStream = tcpClient.GetStream();
                    byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(textToSend);

                    //---send the text---
                    Console.WriteLine("Sending : " + textToSend);
                    nwStream.Write(bytesToSend, 0, bytesToSend.Length);

                    //---read back the text---
                    byte[] bytesToRead = new byte[tcpClient.ReceiveBufferSize];
                    int bytesRead = nwStream.Read(bytesToRead, 0, tcpClient.ReceiveBufferSize);
                    Console.WriteLine("Received : " + Encoding.ASCII.GetString(bytesToRead, 0, bytesRead));
                    Console.ReadLine();
                    tcpClient.Close();
                    return true;
                }
                else
                {
                    tcpClient.Close();
                    // Not connected
                    Console.WriteLine("TCP not available : " + hostEndPoint);
                    return false;
                }                             
            }
            catch (Exception e)
            {
                Console.WriteLine("Error..... " + e.StackTrace);
                return false;
            }
        }

        public void ReceiveMainWindow(MainWindow m)
        {
            main = m;
        }

        private IPEndPoint ConvertStringToIPEndPoint(string endPoint)
        {
            string[] ep = endPoint.Split(':');
            if (ep.Length != 2) throw new FormatException("Invalid endpoint format");
            IPAddress ip2;
            if (!IPAddress.TryParse(ep[0], out ip2))
            {
                throw new FormatException("Invalid ip-adress");
            }
            int port;
            if (!int.TryParse(ep[1], NumberStyles.None, NumberFormatInfo.CurrentInfo, out port))
            {
                throw new FormatException("Invalid port");
            }

            IPEndPoint hostEndPoint = new IPEndPoint(ip2, port);

            return hostEndPoint;
        }

    }
}
