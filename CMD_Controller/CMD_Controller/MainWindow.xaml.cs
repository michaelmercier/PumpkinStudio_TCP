﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;


namespace CMD_Controller
{
    public partial class MainWindow : Window
    {
        private int NbPlayer = 0;
        private int OpenServer_Time = 30; // 35 secondes
        public int nbIP;
        public int TotalPlayer = 4;
        private int CountPlayer = 0;
        private int timerInSecond = 0;
        private int timerAddSecond = 0;

        private string LanguagePath;
        public string PathVideo1;
        public string PathVideo2;
        private string[] GamePath;
        public string[] IpList;
        private const string KILL_VLC = "Taskkill /F /IM vlc.exe";
        private const string OPENVLC = "vlc --fullscreen --play-and-exit ";
        private const string OPENVLC_LOOP = "vlc --fullscreen --loop ";

        private bool GameStarted = false;
        private bool AppFinishedLoading = false;
        private bool StartedVideo1 = false;
        private bool StartedVideo2 = false;
        private bool firstTickValidation = true;

        private List<bool> ButtonClickedList = new List<bool>();
        private List<bool> ComputerReadyList = new List<bool>();
        private List<Image> ImgButtonList = new List<Image>();
        private List<Image> ImgConnectionList = new List<Image>();
        private List<string> ImgPathBlackBtn = new List<string>();
        private List<string> ImgPathBlueBtn = new List<string>();
        private List<string> ImgNotConnected = new List<string>();
        private List<string> ImgConnected = new List<string>();
        private List<ComboBox> CBList = new List<ComboBox>();
        private List<Button> BtnPlayerList = new List<Button>();
        private List<Button> BtnOptionList = new List<Button>();

        //tick
        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer dispatcherValidation = new System.Windows.Threading.DispatcherTimer();

        //Initalize classes.
        ProcessCDM cmd = new ProcessCDM();
        SettingsWindow setwin = new SettingsWindow();
        TCP_Controller tcp = new TCP_Controller();

        public MainWindow()
        {
            InitializeComponent();
        }

        //When the window is loaded. 
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cmd.StartCMD();
            ReadFileIP();
            InitializeLists();
            ReadFileGamePath();
            SetComboBox();
            Timer();
            TimerValidation();

            AppFinishedLoading = true;
            setwin.ReceiveMainWindow(this);
        }

        private void InitializeLists()
        {
            CBList.Add(cbPlayer1);
            CBList.Add(cbPlayer2);
            CBList.Add(cbPlayer3);
            CBList.Add(cbPlayer4);

            BtnPlayerList.Add(bp1);
            BtnPlayerList.Add(bp2);
            BtnPlayerList.Add(bp3);
            BtnPlayerList.Add(bp4);
            
            ImgButtonList.Add(imgBtn1);
            ImgButtonList.Add(imgBtn2);
            ImgButtonList.Add(imgBtn3);
            ImgButtonList.Add(imgBtn4);

            ImgConnectionList.Add(imgP1Connection);
            ImgConnectionList.Add(imgP2Connection);
            ImgConnectionList.Add(imgP3Connection);
            ImgConnectionList.Add(imgP4Connection);

            ImgPathBlackBtn.Add(@"/Images/I_Button_Main_01_Black01.png");
            ImgPathBlackBtn.Add(@"/Images/I_Button_Main_01_Black02.png");
            ImgPathBlackBtn.Add(@"/Images/I_Button_Main_01_Black03.png");
            ImgPathBlackBtn.Add(@"/Images/I_Button_Main_01_Black04.png");

            ImgPathBlueBtn.Add(@"/Images/AI_Button_Main_01_Blue01.png");
            ImgPathBlueBtn.Add(@"/Images/AI_Button_Main_01_Blue02.png");
            ImgPathBlueBtn.Add(@"/Images/AI_Button_Main_01_Blue03.png");
            ImgPathBlueBtn.Add(@"/Images/AI_Button_Main_01_Blue04.png");

            ImgNotConnected.Add(@"/Images/P_Button_Main_04_Black_01.png");
            ImgNotConnected.Add(@"/Images/P_Button_Main_04_Black_02.png");
            ImgNotConnected.Add(@"/Images/P_Button_Main_04_Black_03.png");
            ImgNotConnected.Add(@"/Images/P_Button_Main_04_Black_04.png");

            ImgConnected.Add(@"/Images/P_Button_Main_04_Blue_01.png");
            ImgConnected.Add(@"/Images/P_Button_Main_04_Blue_02.png");
            ImgConnected.Add(@"/Images/P_Button_Main_04_Blue_03.png");
            ImgConnected.Add(@"/Images/P_Button_Main_04_Blue_04.png");

            BtnOptionList.Add(bStart);
            BtnOptionList.Add(bStartGame);
            BtnOptionList.Add(setwin.bShutDown);

            //Vadation of the buttons for the players.
            for (int i = 0; i < 4; i++)
            {
                ButtonClickedList.Add(false);
            }
        }

        /// <summary>
        /// Number of Player that will play the game
        /// </summary>
        private void bp1_Click(object sender, RoutedEventArgs e)
        {
            int i = 0;
            SetButton(i);
        }

        private void bp2_Click(object sender, RoutedEventArgs e)
        {
            int i = 1;
            SetButton(i);
        }

        private void bp3_Click(object sender, RoutedEventArgs e)
        {
            int i = 2;
            SetButton(i);
        }

        private void bp4_Click(object sender, RoutedEventArgs e)
        {
            int i = 3;
            SetButton(i);
        }

        /// <summary>
        /// Disable buttons when the game is started. 
        /// </summary>
        private void EnableDisableButton(bool option)
        {
            foreach (var item in BtnPlayerList)
            {
                item.IsEnabled = option;
            }

            foreach (var item in CBList)
            {
                item.IsEnabled = option;
            }
        }

        private void EnableDisableOptionButton(bool option)
        {
                foreach (var item in BtnOptionList)
                {
                    item.IsEnabled = option;
                }
        }

        /// <summary>
        /// Set the comboBox with the available language & set it to the first one by default.(chinese)
        /// </summary>
        private void SetComboBox()
        {
            List<string> myItemsCollection = new List<string>();
            myItemsCollection.Add("中国語");
            myItemsCollection.Add("韓国語");
            myItemsCollection.Add("日本語");
            myItemsCollection.Add("英語");

            foreach (var item in CBList)
            {
                item.ItemsSource = myItemsCollection;
                item.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Set the button images and the count of the players. 
        /// </summary>
        private void SetButton(int i)
        {
            if (i + 1 <= nbIP)
            {
                try
                {
                    if (!ButtonClickedList[i])
                    {
                        NbPlayer++;
                        ImgButtonList[i].Source = new BitmapImage(new Uri(ImgPathBlueBtn[i], UriKind.Relative));
                        ButtonClickedList[i] = true;
                    }
                    else
                    {
                        NbPlayer--;
                        ImgButtonList[i].Source = new BitmapImage(new Uri(ImgPathBlackBtn[i], UriKind.Relative));
                        ButtonClickedList[i] = false;
                    }
                }
                catch { }
            }
        }

        /// <summary>
        /// Open game. 
        /// </summary>
        private void bStart_Click(object sender, RoutedEventArgs e)
        {
            if (GameStarted != true)
            {
                if (NbPlayer != 0)
                {
                    imgOpen.Source = new BitmapImage(new Uri(@"/Images/P_Button_Open_Blue_IMG.png", UriKind.Relative));

                    EnableDisableButton(false);
                    EnableDisableOptionButton(false);

                    GameStarted = true;

                    for (int i = 0; i < TotalPlayer; i++)
                    {
                        if (ButtonClickedList[i])
                        {
                            CountPlayer++;
                        }
                    }

                    if (CountPlayer == 1)
                        timerInSecond = 40;
                    else
                        timerInSecond = 60;

                    dispatcherTimer.Start();

                }
                else
                {
                    MessageBox.Show("No player selected !", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            else
            {
                MessageBox.Show("The game is already started !", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        /// <summary>
        /// CLose all the games that are opened.
        /// </summary>
        private void bStop_Click(object sender, RoutedEventArgs e)
        {
                imgClose.Source = new BitmapImage(new Uri(@"/Images/P_Button_Stop_Blue.png", UriKind.Relative));
                MessageBoxResult result = MessageBox.Show("Do you want to close the game ?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    EnableDisableButton(true);
                    dispatcherTimer.Stop();
                    ResetTick();

                    imgOpen.Source = new BitmapImage(new Uri(@"/Images/P_Button_Open_Black.png", UriKind.Relative));
                    imgStart.Source = new BitmapImage(new Uri(@"/Images/P_Button_Start_Black.png", UriKind.Relative));

                    for (int i = 0; i < TotalPlayer; i++)
                    {
                        if (ButtonClickedList[i])
                        {
                             SendDataToClient(cmd.KILL_GAME, IpList[i]);
                        }
                    }

                    GameStarted = false;

                    imgClose.Source = new BitmapImage(new Uri(@"/Images/P_Button_Stop_Black.png", UriKind.Relative));
               }
                imgClose.Source = new BitmapImage(new Uri(@"/Images/P_Button_Stop_Black.png", UriKind.Relative));
        }

        /// <summary>
        /// Start the game with a .wfs file that will simulate keyboard. (Number of player + enter)
        /// </summary>
        private void bStartGame_Click(object sender, RoutedEventArgs e)
        {
            if (GameStarted == true)
            {
                ClickOnGameRequest();

                imgStart.Source = new BitmapImage(new Uri(@"/Images/P_Button_Start_Blue.png", UriKind.Relative));
                string sFile = "";
                sFile = "call " + NbPlayer;

                bool b = false;
                for (int i = 0; i < TotalPlayer; i++)
                {
                    if (ButtonClickedList[i])
                    {
                        if (!b)
                        {
                            SendDataToClient(sFile, IpList[i]);
                            b = true;
                        }               
                    }
                }
            }
            else
                MessageBox.Show("The game isn't open!", "Warning", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        /// <summary>
        /// Open a video2
        /// </summary>
        private void btnVideo2_Click(object sender, RoutedEventArgs e)
        {
            if ((!StartedVideo2 && setwin.DefaultVideoPlayer <= nbIP) || (!StartedVideo2 && setwin.DefaultVideoPlayer == 5))
            {
                if (setwin.DefaultVideoPlayer == 0)
                    cmd.ExecuteCommands(OPENVLC + PathVideo2);
                else if (setwin.DefaultVideoPlayer == 5)
                {
                    List<int> languages = CheckLanguageVideo();
                    int i = 0;
                    foreach (var list in IpList)
                    {
                        SendDataToClient(OPENVLC + "--audio-track=" + languages[i] + " " + PathVideo2, list);
                        i++;
                    }
                }
                else
                    SendDataToClient(OPENVLC + PathVideo2, IpList[setwin.DefaultVideoPlayer - 1]);

                StartedVideo2 = true;
                imgVideo2.Source = new BitmapImage(new Uri(@"/Images/Second_Blue_Video_01.png", UriKind.Relative));
                setwin.btnVideoSelection1.IsEnabled = false;
                btnVideo1.IsEnabled = false;
            }
            else if (StartedVideo2)
            {
                if (setwin.DefaultVideoPlayer == 0)
                    cmd.ExecuteCommands(KILL_VLC);
                else if (StartedVideo2 && setwin.DefaultVideoPlayer == 5)
                {
                    foreach (var list in IpList)
                    {
                        SendDataToClient(KILL_VLC, list);
                    }
                }
                else
                    SendDataToClient(KILL_VLC, IpList[setwin.DefaultVideoPlayer - 1]);

                StartedVideo2 = false;
                imgVideo2.Source = new BitmapImage(new Uri(@"/Images/Second_Black_Video_01.png", UriKind.Relative));
                setwin.btnVideoSelection1.IsEnabled = true;
                btnVideo1.IsEnabled = true;
            }
        }

        private List<int> CheckLanguageVideo()
        {
            List<string> languages = new List<string>();
            languages.Add(cbPlayer1.SelectedItem.ToString());
            languages.Add(cbPlayer2.SelectedItem.ToString());
            languages.Add(cbPlayer3.SelectedItem.ToString());
            languages.Add(cbPlayer4.SelectedItem.ToString());

            List<int> langOrder = new List<int>();

            foreach (var lan in languages)
            {
                int i = 0;
                switch (lan)
                {
                    case "中国語":
                        i = 0;
                        break;
                    case "韓国語":
                        i = 1;
                        break;
                    case "日本語":
                        i = 2;
                        break;
                    case "英語":
                        i = 3;
                        break;
                }
                langOrder.Add(i);
            }
            return langOrder;
        }

        /// <summary>
        /// Open a video1
        /// </summary>
        private void btnVideo1_Click(object sender, RoutedEventArgs e)
        {
            if (!StartedVideo1 && setwin.DefaultVideoPlayer <= nbIP)
            {
                if (setwin.DefaultVideoPlayer == 0)
                    cmd.ExecuteCommands(OPENVLC_LOOP + PathVideo1);
                else
                    tcp.MainTCP(OPENVLC_LOOP + PathVideo1, IpList[setwin.DefaultVideoPlayer - 1]);

                StartedVideo1 = true;
                imgVideo1.Source = new BitmapImage(new Uri(@"/Images/Second_Blue_Video_02.png", UriKind.Relative));
                setwin.btnVideoSelection1.IsEnabled = false;
                btnVideo2.IsEnabled = false;
            }
            else if (StartedVideo1)
            {
                if (setwin.DefaultVideoPlayer == 0)
                    cmd.ExecuteCommands(KILL_VLC);
                else
                    tcp.MainTCP(KILL_VLC, IpList[setwin.DefaultVideoPlayer - 1]);
                StartedVideo1 = false;
                imgVideo1.Source = new BitmapImage(new Uri(@"/Images/Second_Black_Video_02.png", UriKind.Relative));
                setwin.btnVideoSelection1.IsEnabled = true;
                btnVideo2.IsEnabled = true;
            }

        }

        /// <summary>
        /// Read .txt File to get the Ip adresses.
        /// </summary>
        private void ReadFileIP()
        {
            var list = new List<string>();
            var fileStream = new FileStream(System.IO.Path.GetFullPath("ConnectedComputer.txt"), FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    list.Add(line);
                }
            }
            IpList = list.ToArray();
            nbIP = IpList.Count();
        }

        /// <summary>
        /// Get the path of the game and insert it into a label.
        /// </summary>
        private void ReadFileGamePath()
        {
            GamePath = cmd.ReadFileGamePath();
            //Initialize values in cmd with the game path.
            cmd.InitializeValues();

            try
            {
                LanguagePath = GamePath[1];
                PathVideo1 = GamePath[2];
                PathVideo2 = GamePath[3];
            }
            catch { }
        }

        /// <summary>
        /// Set languages for all players.
        /// </summary>
        private void cbPlayer1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string SelectedLanguage = cbPlayer1.SelectedItem.ToString();
            SelectedLanguage = TranslateLanguageSelection(SelectedLanguage);
            DefaultLanguage(SelectedLanguage, 0);       
        }

        private void cbPlayer2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string SelectedLanguage = cbPlayer2.SelectedItem.ToString();
            SelectedLanguage = TranslateLanguageSelection(SelectedLanguage);
            DefaultLanguage(SelectedLanguage, 1);
        }

        private void cbPlayer3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string SelectedLanguage = cbPlayer3.SelectedItem.ToString();
            SelectedLanguage = TranslateLanguageSelection(SelectedLanguage);
            DefaultLanguage(SelectedLanguage, 2);       
        }

        private void cbPlayer4_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string SelectedLanguage = cbPlayer4.SelectedItem.ToString();
            SelectedLanguage = TranslateLanguageSelection(SelectedLanguage);
            DefaultLanguage(SelectedLanguage, 3);
        }

        private string TranslateLanguageSelection( string s)
        {
            if (s == "中国語")
            {
                s = "Chinese";
            }
            else if (s == "韓国語")
            {
                s = "Korean";
            }
            else if (s == "日本語")
            {
                s = "Japanese";
            }
            else if (s == "英語")
            {
                s = "English";
            }

            return s;
        }

        /// <summary>
        /// Default language before starting the game. (Chinese)
        /// </summary>
        public void DefaultLanguage(string language, int i)
        {
            if (AppFinishedLoading == true)
            {
                try
                {
                    string LanguageInfo = language + "|" + LanguagePath;
                    SendDataToClient(LanguageInfo, IpList[i]);
                }
                catch { }
            }
            else
                try
                {
                    SendDataToClient("Chinese" + "|" + LanguagePath, IpList[i]);
                }
                catch { }
        }

        /// <summary>
        /// Start the tick event for the timer
        /// </summary>
        private void Timer()
        {
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
        }
        
        /// <summary>
        /// Use the timer tick to have a timer and open the game on the time wanted.
        /// </summary>
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            txtOpen.Text = timerInSecond.ToString();
            bool failed = false;

            try
            {
                if (CountPlayer == 1)
                {
                    bool b;
                    if (timerAddSecond == 0)
                    {
                        b = OpenGameRequest();

                        if (!b)
                        {
                            failed = true;
                            ResetTick();
                            EnableDisableButton(true);
                            GameStarted = false;
                            imgOpen.Source = new BitmapImage(new Uri(@"/Images/P_Button_Open_Black.png", UriKind.Relative));
                            MessageBox.Show("Connection failed.\rThe server isn't able to open the game.\rSelect another server or check the client's information.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }  
                }
                else if (CountPlayer > 1)
                {
                    if (timerAddSecond == 0)
                    {
                        bool firstIP = true;
                        for (int i = 0; i < TotalPlayer; i++)
                        {
                            if (ButtonClickedList[i] && firstIP)
                            {
                                bool b = SendDataToClient(cmd.EXECUTE_GAME, IpList[i]);
                                firstIP = false;

                                if (!b)
                                {
                                    failed = true;
                                    ResetTick();
                                    EnableDisableButton(true);
                                    GameStarted = false;
                                    imgOpen.Source = new BitmapImage(new Uri(@"/Images/P_Button_Open_Black.png", UriKind.Relative));
                                    MessageBox.Show("Connection failed.\rThe server isn't able to open the game.\rSelect another server or check the client's information.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                                }                             
                            }
                        }
                    }
                    else if (timerAddSecond == OpenServer_Time)
                    {
                        bool firstIP = true;
                        for (int i = 0; i < TotalPlayer; i++)
                        {
                            if (ButtonClickedList[i])
                            {
                                if (!firstIP)
                                    SendDataToClient(cmd.EXECUTE_GAME, IpList[i]);

                                firstIP = false;
                            }
                        }
                    }
                }
            }
            catch { }

            if (timerInSecond == 0)
            {
                ResetTick();
                if(!failed)
                imgOpen.Source = new BitmapImage(new Uri(@"/Images/P_Button_Open_Blue.png", UriKind.Relative));
            }
            else
            {
                timerInSecond--;
                timerAddSecond++;
            }
        }

        private void ResetTick()
        {
            txtOpen.Text = "";
            timerInSecond = 0;
            CountPlayer = 0;
            timerAddSecond = 0;
            EnableDisableOptionButton(true);
            dispatcherTimer.Stop();
        }

        private bool OpenGameRequest()
        {
            bool b = false;
            for (int i = 0; i < TotalPlayer; i++)
            {
                if (ButtonClickedList[i])
                {
                    b = SendDataToClient(cmd.EXECUTE_GAME, IpList[i]);
                }
            }
            return b;
        }

        private void ClickOnGameRequest()
        {
            for (int i = 0; i < TotalPlayer; i++)
            {
                if (ButtonClickedList[i])
                {
                    SendDataToClient(GamePath[0] + " +", IpList[i]);
                }
            }
        }

        private void btnSetting_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                setwin.Show();
            }
            catch { }
        }

        public void TimerValidation()
        {
            dispatcherValidation.Tick += dispatcherValidation_Tick;
            dispatcherValidation.Interval = new TimeSpan(0, 0, 1);
            dispatcherValidation.Start();
        }

        /// <summary>
        /// Use the timer tick to have a timer and open the game on the time wanted.
        /// </summary>
        private void dispatcherValidation_Tick(object sender, EventArgs e)
        {
            bool b;
            for (int i = 0; i < IpList.Length; i++)
            {
                b = tcp.MainTCP("CheckUp", IpList[i]);

                if (!b)
                    ImgConnectionList[i].Source = new BitmapImage(new Uri(ImgNotConnected[i], UriKind.Relative));
                else
                    ImgConnectionList[i].Source = new BitmapImage(new Uri(ImgConnected[i], UriKind.Relative));
            }

            if (firstTickValidation)
            {
                dispatcherValidation.Interval = new TimeSpan(0, 0, 10);
                firstTickValidation = false;
            }
        }

        private bool SendDataToClient(string command, string ip)
        {
            bool validation;

            validation = tcp.MainTCP(command,ip);

            if (!validation)
            {
                int i = 1;
                while (!validation)
                {
                    if (i > 2)
                        break;
                    validation = tcp.MainTCP(command, ip);
                    i++;
                }
            }
            return validation;   
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
