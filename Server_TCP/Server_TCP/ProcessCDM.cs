﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using WindowsInput;
using WindowsInput.Native;

namespace Server_TCP
{
    public class ProcessCDM
    {
        Process cmdProcess;
        StreamWriter cmdStreamWriter;

        private static StringBuilder cmdOutput = null;
        private const string KILL_APP = "Taskkill /F /IM Server_TCP.exe";
        private const string SHUTDOWN = "shutdown -s";

        //Mouse actions
        private const int MOUSEEVENTF_LEFTDOWN = 0x02;
        private const int MOUSEEVENTF_LEFTUP = 0x04;
        private const int MOUSEEVENTF_RIGHTDOWN = 0x08;
        private const int MOUSEEVENTF_RIGHTUP = 0x10;

        //DLLs for the mouse event & to change the main screen.
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint cButtons, uint dwExtraInfo);
        [DllImport("user32")]
        private static extern bool SetForegroundWindow(IntPtr hwnd);

        public void StartCMD()
        {
            cmdOutput = new StringBuilder("");
            cmdProcess = new Process();

            cmdProcess.StartInfo.FileName = "cmd.exe";
            cmdProcess.StartInfo.UseShellExecute = false;
            cmdProcess.StartInfo.CreateNoWindow = true;
            cmdProcess.StartInfo.RedirectStandardOutput = true;

            cmdProcess.OutputDataReceived += new DataReceivedEventHandler(SortOutputHandler);
            cmdProcess.StartInfo.RedirectStandardInput = true;
            cmdProcess.Start();

            cmdStreamWriter = cmdProcess.StandardInput;
            cmdProcess.BeginOutputReadLine();
        }

        /// <summary>
        /// Execute/Kill the application.
        /// </summary>
        /// 
        public void ExecuteCommands(string sCommand)
        {
            cmdStreamWriter.WriteLine(sCommand);
        }

        public void KillApp()
        {
            cmdStreamWriter.WriteLine(KILL_APP);
        }

        public void ShutdownComputer()
        {
            cmdStreamWriter.WriteLine(SHUTDOWN);
        }

        /// <summary>
        /// Output of the CMD.
        /// </summary>
        private static void SortOutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            if (!String.IsNullOrEmpty(outLine.Data))
            {
                cmdOutput.Append(Environment.NewLine + outLine.Data);
            }
        }

        /// <summary>
        /// Read file and write into it the new languge. This will be done at the startup.
        /// </summary>
        public void ChangeGameLanguage(string data)
        {
            try
            {
                string line;
                string[] lines;
                string[] DataSplited = data.Split('|');
                string LanguageFilePath = DataSplited[1];
                var list = new List<string>();

                var fileStream = new FileStream(LanguageFilePath, FileMode.Open, FileAccess.Read);
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        list.Add(line);
                    }
                }

                lines = list.ToArray();
                string LanguageSelected = CheckLanguageSelected(DataSplited[0]);
                lines[1] = "Language=" + LanguageSelected;

                File.WriteAllText(LanguageFilePath, string.Empty);

                var fileWriter = new FileStream(LanguageFilePath, FileMode.Open, FileAccess.Write);
                using (StreamWriter writer = new StreamWriter(fileWriter))
                {
                    writer.WriteLine(lines[0]);
                    writer.Write(lines[1]);
                }
            }
            catch { }
        }

        /// <summary>
        /// Select which languge to add to the language file.
        /// </summary>
        private string CheckLanguageSelected(string language)
        {
            string LanguageSelected = "";

            if (language == "Chinese")  
                LanguageSelected = "SEGA_CN";
            
            else if (language == "Korean")
                LanguageSelected = "Korea";

            else if (language == "Japanese")
                LanguageSelected = "SEGA_JP";

            else if (language == "English")
                LanguageSelected = "SEGA_EN";

            return LanguageSelected;
        }

        /// <summary>
        /// This function include to change the game for the main window and to random click on the screen to select the game.
        /// </summary>
        public void SelectGameWindow(string application)
        {
            string[] FirstSplit = application.Split('\\');
            int lenght = FirstSplit.Length-1;
            string[] SecondSplit = FirstSplit[lenght].Split('.');

            var proc = Process.GetProcessesByName(SecondSplit[0]).FirstOrDefault();
            if (proc != null && proc.MainWindowHandle != IntPtr.Zero)
            {
                //Set the game window as the main and first one.
                SetForegroundWindow(proc.MainWindowHandle);

                //Click on the screen to select the game. Some PCs have issues about changing the main window.
                uint X = (uint)Cursor.Position.X;
                uint Y = (uint)Cursor.Position.Y;
                mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, X, Y, 0, 0);              
            }
        }

        public void StartGame(string info)
        {
            VirtualKeyCode key = VirtualKeyCode.NUMPAD1;
            string[] splitInfo = info.Split(' ');

            switch (Convert.ToInt32(splitInfo[1]))
            {
                case 1:                   
                    break;
                case 2:
                    key = VirtualKeyCode.NUMPAD2;
                    break;
                case 3:
                    key = VirtualKeyCode.NUMPAD3;
                    break;
                case 4:
                    key = VirtualKeyCode.NUMPAD4;
                    break;
            }
 
            InputSimulator input = new InputSimulator();
            input.Keyboard.KeyDown(key);
            input.Keyboard.KeyUp(key);
            input.Keyboard.KeyDown(VirtualKeyCode.RETURN);
            input.Keyboard.KeyUp(VirtualKeyCode.RETURN);
        }
    }
}
