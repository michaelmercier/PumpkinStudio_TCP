﻿using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace Server_TCP
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const int PORT_NO = 5000;
        private string SERVER_IP;

        ProcessCDM cmd = new ProcessCDM();

        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Looping server waiting for informations to come in. 
        /// </summary>
        public void serverThread()
        {
            while (true)
            {
                try
                {
                    //---listen at the specified IP and port no.---
                    IPAddress localAdd = IPAddress.Parse(SERVER_IP);
                    TcpListener listener = new TcpListener(localAdd, PORT_NO);
                    Console.WriteLine("Listening...");
                    listener.Start();

                    //---incoming client connected---
                    TcpClient client = listener.AcceptTcpClient();

                    //---get the incoming data through a network stream---
                    NetworkStream nwStream = client.GetStream();
                    byte[] buffer = new byte[client.ReceiveBufferSize];

                    //---read incoming stream---
                    int bytesRead = nwStream.Read(buffer, 0, client.ReceiveBufferSize);

                    //---convert the data received into a string---
                    string dataReceived = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                    Console.WriteLine("Received : " + dataReceived);

                    if (dataReceived == "CheckUp")
                    { }
                    else if (dataReceived.Contains('|'))
                        cmd.ChangeGameLanguage(dataReceived);

                    else if (dataReceived == "Shutdown")
                        cmd.ShutdownComputer();

                    else if (dataReceived.Contains('+'))
                        cmd.SelectGameWindow(dataReceived);

                    else if (dataReceived.Contains("call"))
                        cmd.StartGame(dataReceived);

                    else
                        cmd.ExecuteCommands(dataReceived);


                    byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes("OK");
                    //---write back the text to the client---
                    Console.WriteLine("Sending back : " + bytesToSend);
                    //nwStream.Write(buffer, 0, bytesToSend);
                    nwStream.Write(bytesToSend, 0, bytesToSend.Length);
                    client.Close();
                    listener.Stop();
                    Console.ReadLine();

                }
                catch (Exception e)
                {
                    Console.WriteLine("Error..... " + e.StackTrace);
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GetIpAdress();
            cmd.StartCMD();

            Thread thdTCPServer = new Thread(new ThreadStart(serverThread));
            thdTCPServer.Start();

            WindowState = System.Windows.WindowState.Minimized;
            this.Hide();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            cmd.KillApp();
            Application.Current.Shutdown();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            cmd.KillApp();
            Application.Current.Shutdown();
        }

        private void GetIpAdress()
        {
            IPAddress IP = Dns.GetHostEntry(Dns.GetHostName()).AddressList.FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
            SERVER_IP = IP.ToString();
        }
    }
}
